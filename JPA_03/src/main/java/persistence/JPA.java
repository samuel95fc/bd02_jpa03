package persistence;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class JPA {

	public static void main(String[] args) {
		EntityManagerFactory emf = null;
		try {
			emf = Persistence.createEntityManagerFactory("teste");
			EntityManager em = emf.createEntityManager();
			
			popular(em);
			
			em.clear();

		}
		catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (emf != null) {
				emf.close();
			}
		}

	}
	
	private static void popular(EntityManager em) {
		em.getTransaction().begin();

		Veiculo veiculo = ;
		Pessoa pessoa = ;
		Modelo modelo = ModeloDAO.insert(modelo);

		em.getTransaction().commit();
	}
}