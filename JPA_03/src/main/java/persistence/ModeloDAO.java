package persistence;

import java.util.ArrayList;
import java.util.List;

import entity.Modelo;
import exception.ModeloNaoEncontradoException;

public class ModeloDAO {
	
	private static List<Modelo> modelos = new ArrayList<>();

	public static Modelo obterPorCodigo(Integer codigo) throws ModeloNaoEncontradoException {
		System.out.println("Modelo");
		for (Modelo modelo : modelos) {
			if (modelo.getCodigo().equals(codigo)) {
				return modelo;
			}
		}
		throw new ModeloNaoEncontradoException();
	}
	
	public static void insert(Modelo modelo){
		modelos.add(modelo);
	}
}