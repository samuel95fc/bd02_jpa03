package persistence;

import java.util.ArrayList;
import java.util.List;

import entity.Pessoa;
import exception.PessoaNaoEncontradaException;

public class PessoaDAO {
	
	private static List<Pessoa> pessoas = new ArrayList<>();

	public static Pessoa obterPorCpf(String cpf) throws PessoaNaoEncontradaException {
		System.out.println("Pessoa");
		for (Pessoa pessoa : pessoas) {
			if (pessoa.getCpf().equalsIgnoreCase(cpf)) {
				return pessoa;
			}
		}
		throw new PessoaNaoEncontradaException();
	}
	
	public static void insert(Pessoa pessoa){
		pessoas.add(pessoa);
	}


}