package entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "tab_veiculo")
public class Veiculo {
	
	@Id
	@Column(length = 7, nullable = false)
	private String placa;
	
	@Column(length = 17, nullable = false)
	private String chassis;
	
	@Column(nullable = false)
	private Modelo modelo;
	
	@Enumerated(EnumType.STRING)
	private TipoVeiculoEnum tipo;
	
	@Column(nullable = false)
	private Date data_Compra;
	
	@Column(nullable = false)
	private Pessoa comprador;

	public Veiculo(String placa, String chassis, Modelo modelo, TipoVeiculoEnum tipo, Date data_Compra,
			Pessoa comprador) {
		super();
		this.placa = placa;
		this.chassis = chassis;
		this.modelo = modelo;
		this.tipo = tipo;
		this.data_Compra = data_Compra;
		this.comprador = comprador;
	}

	public String getPlaca() {
		return placa;
	}

	public void setPlaca(String placa) {
		this.placa = placa;
	}

	public String getChassis() {
		return chassis;
	}

	public void setChassis(String chassis) {
		this.chassis = chassis;
	}

	public Modelo getModelo() {
		return modelo;
	}

	public void setModelo(Modelo modelo) {
		this.modelo = modelo;
	}

	public TipoVeiculoEnum getTipo() {
		return tipo;
	}

	public void setTipo(TipoVeiculoEnum tipo) {
		this.tipo = tipo;
	}

	public Date getData_Compra() {
		return data_Compra;
	}

	public void setData_Compra(Date data_Compra) {
		this.data_Compra = data_Compra;
	}

	public Pessoa getComprador() {
		return comprador;
	}

	public void setComprador(Pessoa comprador) {
		this.comprador = comprador;
	}

}