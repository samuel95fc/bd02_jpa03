package entity;

public enum TipoVeiculoEnum {
	CARGA ("CR"), PASSEIO ("PS");
	
	private String codigo;

	private TipoVeiculoEnum(String codigo) {

		this.codigo = codigo;
	}

	public String getCodigo() {
		return codigo;
	}

	public static TipoVeiculoEnum valueOfCodigo(String codigo) {
		for (TipoVeiculoEnum tipoVeiculoEnum : values()) {
			if (tipoVeiculoEnum.getCodigo().equals(codigo)) {
				return tipoVeiculoEnum;
			}
		}
		throw new IllegalArgumentException();
	}


}
