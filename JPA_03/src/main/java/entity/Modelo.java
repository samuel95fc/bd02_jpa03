package entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "tab_modelo")
public class Modelo {
	
	@Id
	@Column(nullable = false)
	private Integer codigo;
	
	@Column(length = 50, nullable = false)
	private String nome;

	public Integer getCodigo() {
		return codigo;
	}

	public Modelo(Integer codigo, String nome) {
		super();
		this.codigo = codigo;
		this.nome = nome;
	}

	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
}
